# ATM machines allow 4 or 6 digit PIN codes and PIN codes cannot contain anything but exactly 4 digits or exactly 6 digits.
# If the function is passed a valid PIN string, return true, else return false.

# My solution
def validate_pin(pin):
        if (len(pin) == 4 or len(pin) ==6):
            for i in pin:
                flag = True
                if i not in ['1','2','3','4','5','6','7','8','9','0']:                
                    flag = False
                    break;
            return flag
        else:
            return False

# Best practice
def validate_pin(pin):
    return len(pin) in (4, 6) and pin.isdigit()